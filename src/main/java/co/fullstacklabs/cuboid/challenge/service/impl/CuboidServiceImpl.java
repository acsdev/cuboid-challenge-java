package co.fullstacklabs.cuboid.challenge.service.impl;

import co.fullstacklabs.cuboid.challenge.dto.CuboidDTO;
import co.fullstacklabs.cuboid.challenge.exception.ResourceNotFoundException;
import co.fullstacklabs.cuboid.challenge.exception.UnprocessableEntityException;
import co.fullstacklabs.cuboid.challenge.model.Bag;
import co.fullstacklabs.cuboid.challenge.model.Cuboid;
import co.fullstacklabs.cuboid.challenge.repository.BagRepository;
import co.fullstacklabs.cuboid.challenge.repository.CuboidRepository;
import co.fullstacklabs.cuboid.challenge.service.CuboidService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Validator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation class for BagService
 *
 * @author FullStack Labs
 * @version 1.0
 * @since 2021-10-22
 */
@Service
public class CuboidServiceImpl implements CuboidService {

    private final CuboidRepository repository;
    private final BagRepository bagRepository;
    private final ModelMapper mapper;
    private final Validator validator;

    @Autowired
    public CuboidServiceImpl(@Autowired CuboidRepository repository,
                             BagRepository bagRepository, ModelMapper mapper, Validator validator) {
        this.repository = repository;
        this.bagRepository = bagRepository;
        this.mapper = mapper;
        this.validator = validator;
    }

    /**
     * Create a new cuboid and add it to its bag checking the bag available capacity.
     *
     * @param cuboidDTO DTO with cuboid properties to be persisted
     * @return CuboidDTO with the data created
     */
    @Override
    @Transactional
    public CuboidDTO create(CuboidDTO cuboidDTO) {
        Bag bag = getBagById(cuboidDTO.getBagId());
        Cuboid cuboid = mapper.map(cuboidDTO, Cuboid.class);
        cuboid.setBag(bag);
        bag.addCuboid(cuboid);
        if (validator.validate(bag).size() > 0) {
            throw new UnprocessableEntityException("Not enough capacity");
        }
        cuboid = repository.save(cuboid);
        return mapper.map(cuboid, CuboidDTO.class);
    }

    /**
     * Performs an update operation
     * @param id identification of cuboid
     * @param cuboidDTO data to override cuboid
     * @return DTO with update data
     */
    @Override
    @Transactional
    public CuboidDTO update(Long id, CuboidDTO cuboidDTO) {
        if (! repository.existsById(id)) {
            throw new ResourceNotFoundException("Cuboid NOT found");
        }
        final Bag bag = getBagById(cuboidDTO.getBagId());
        final Cuboid cuboid = mapper.map(cuboidDTO, Cuboid.class);

        bag.getCuboids().stream()
                .filter(item -> cuboid.getId().equals(item.getId()))
                .findFirst()
                .ifPresent(c -> {
                    c.setWidth(cuboid.getWidth());
                    c.setHeight(cuboid.getHeight());
                    c.setDepth(cuboid.getDepth());
                });


        if (validator.validate(bag).size() > 0) {
            throw new UnprocessableEntityException("Not enough capacity");
        }

        final Cuboid saved = repository.save(cuboid);
        return mapper.map(saved, CuboidDTO.class);
    }

    /**
     * Performs delete
     * @param id identification of cuboid
     */
    @Override
    public void delete(Long id) {
        if (! repository.existsById(id)) {
            throw new ResourceNotFoundException("Cuboid NOT found");
        }
        repository.deleteById(id);
    }

    /**
     * List all cuboids
     * @return List<CuboidDTO>
     */
    @Override
    @Transactional(readOnly = true)
    public List<CuboidDTO> getAll() {
        List<Cuboid> cuboids = repository.findAll();
        return cuboids.stream().map(bag -> mapper.map(bag, CuboidDTO.class))
                .collect(Collectors.toList());
    }

    private Bag getBagById(long bagId) {
        return bagRepository.findById(bagId).orElseThrow(() -> new ResourceNotFoundException("Bag not found"));
    }


  
}
