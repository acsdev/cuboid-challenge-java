package co.fullstacklabs.cuboid.challenge.validatoion;

import co.fullstacklabs.cuboid.challenge.model.Bag;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BagCapacityValidator implements ConstraintValidator<BagCapacityConstraint, Bag> {
    @Override
    public boolean isValid(Bag value, ConstraintValidatorContext context) {
        return ! value.hasNotEnoughCapacity();
    }
}
