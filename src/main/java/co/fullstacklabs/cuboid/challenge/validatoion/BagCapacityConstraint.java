package co.fullstacklabs.cuboid.challenge.validatoion;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BagCapacityValidator.class)
public @interface BagCapacityConstraint {
    String message() default "Not enough capacity";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
